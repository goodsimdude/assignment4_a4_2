/***************************************************************************f******************u************zz*******y**
 * File: EmployeeTask.java
 * Course materials (20W) CST 8277
 * @author Mike Norman
 * @date 2020 02
 * @author Solomon Shleifman
 */
package com.algonquincollege.cst8277.models;

import java.time.LocalDateTime;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * 
 * EmployeeTask class
 *
 */
   
//JPA Annotations here
@Embeddable
public class EmployeeTask {

    /**
     * Class variables
     */
    protected String description;
    protected LocalDateTime taskStart;
    protected LocalDateTime taskEnd;
    protected boolean taskDone;
   

    public EmployeeTask() {
        super();
    }

    /**
     * 
     * @return the value of description
     */
    @Column(name = "TASK_DESCRIPTION")
    public String getDescription() {
        return description;
    }
    
    /**
     * 
     * @param description new value of description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 
     * @return the value of taskStart 
     */
    @Column(name = "TASK_START")
    public LocalDateTime getTaskStart() {
        return taskStart;
    }

    /**
     * 
     * @param taskStart new value of taskStart
     */
    public void setTaskStart(LocalDateTime taskStart) {
        this.taskStart = taskStart;
    }

    /**
     * 
     * @return the value of taskEnd 
     */
    @Column(name = "TASK_END_DATE")
    public LocalDateTime getTaskEnd() {
        return taskEnd;
    }

    /**
     * 
     * @param taskEnd new value of taskEnd
     */
    public void setTaskEnd(LocalDateTime taskEnd) {
        this.taskEnd = taskEnd;
    }

    /**
     * 
     * @return the value of taskDone
     */
    @Column(name = "TASK_DONE")
    public boolean isTaskDone() {
        return taskDone;
    }

    /**
     * 
     * @param taskDone new value of taskDone
     */
    public void setTaskDone(boolean taskDone) {
        this.taskDone = taskDone;
    }
    
    
    
    
    

}