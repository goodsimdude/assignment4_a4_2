/***************************************************************************f******************u************zz*******y**
 * File: AddressPojo.java
 * Course materials (20W) CST 8277
 * @author Mike Norman
 * (Modified) @date 2020 02
 *
 * Copyright (c) 1998, 2009 Oracle. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 and Eclipse Distribution License v. 1.0
 * which accompanies this distribution.
 * The Eclipse Public License is available at http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 * http://www.eclipse.org/org/documents/edl-v10.php.
 *
 * Original @authors dclarke, mbraeuer
 * Modified @author Solomon Shleifman
 */
package com.algonquincollege.cst8277.models;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Simple Address class
 */

//JPA Annotations here
@Entity(name ="Address")
@Table(name = "ADDRESS")
public class AddressPojo extends PojoBase implements Serializable {
    /** explicit set serialVersionUID */
    private static final long serialVersionUID = 1L;

    /**
     * Class variables
     */
    protected String city;
    protected String street;
    protected String state;
    protected String country;
    protected String postal;

    /**
     * JPA requires each @Entity class have a default constructor
     */
    public AddressPojo() {
        super();
    }

    /**
     * 
     * @return the value of city
     */
    public String getCity() {
        return city;
    }
    
    /**
     * 
     * @param city new value for city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * 
     * @return the value of street
     */
    public String getStreet() {
        return street;
    }

    /**
     * 
     * @param street new value for street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * 
     * @return the value of state
     */
    public String getState() {
        return state;
    }

    /**
     * 
     * @param state new value for state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * 
     * @return the value of country
     */
    public String getCountry() {
        return country;
    }

    /**
     * 
     * @param country new value for country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * 
     * @return the value of postal
     */
    public String getPostal() {
        return postal;
    }

    /**
     * 
     * @param postal new value for postal
     */
    public void setPostal(String postal) {
        this.postal = postal;
    }
    
    

}