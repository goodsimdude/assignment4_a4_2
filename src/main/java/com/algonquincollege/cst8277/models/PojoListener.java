/***************************************************************************f******************u************zz*******y**
 * File: PojoListener.java
 * Course materials (20W) CST 8277
 *
 * @author (original) Mike Norman
 * @author Solomon Shleifman
 */
package com.algonquincollege.cst8277.models;

import java.time.LocalDateTime;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

public class PojoListener {

    @PrePersist
    public void onPersist(PojoBase p) {

        LocalDateTime now = LocalDateTime.now();
        p.setCreatedDate(now);
        p.setUpdatedDate(now);
    }

    @PreUpdate
    public void onUpdate(PojoBase p) {

        LocalDateTime now = LocalDateTime.now();
        p.setUpdatedDate(now);
    }
}